package ru.sbl.HomeworkSBL.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

/**
 * Класс вклада хранящий сумму вклада и его активность.
 */
@Entity
@Table(name = "deposits")
public class Deposit
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "balance")
    private double balance;

    @Column(name = "active")
    private boolean active;

    public Deposit() { }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Deposit)) return false;
        Deposit deposit = (Deposit) o;
        return id == deposit.id && Double.compare(deposit.balance, balance) == 0 && active == deposit.active;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance, active);
    }

    @Override
    public String toString()
    {
        return "Deposit{" +
                "id=" + id +
                ", balance=" + balance +
                ", active=" + active +
                '}';
    }
}
